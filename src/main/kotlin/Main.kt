sealed class Action {

    object Inc : Action()

    object Dec : Action()
}

data class State(
    val counter: Int,
) {
    companion object {

        val INITIAL = State(
            counter = 0,
        )
    }
}

fun incReducer(action: Action, state: State) = when(action) {
    is Action.Inc -> state.copy(counter = state.counter + 1)
    else -> state
}

fun decReducer(action: Action, state: State) = when(action) {
    is Action.Dec -> state.copy(counter = state.counter - 1)
    else -> state
}

class SummeryMiddleware : AbstractMiddleware<Action, State>() {

    override fun invoke(store: Store<Action, State>, next: Dispatcher<Action, State>, action: Action): State {
        return if (action is Action.Inc && store.state.counter == 5) {
            store.state.copy()
        } else {
            next(action)
        }
    }
}


fun main() {
    val loggerMiddleware = createMiddleware<Action, State> { _, next, action ->
        println(":: logger :: action / ${action::class.qualifiedName}")
        next(action)
    }

    val summerMiddleware = SummeryMiddleware()

    val store = makeStore(
        reducers = linkedSetOf(::incReducer, ::decReducer,),
        preloadedState = State.INITIAL,
        storeEnhancer = applyMiddleware(
            loggerMiddleware,
            summerMiddleware,
        )
    )

    println("Initial state: ${store.state}")

    (0..10).forEach {
        println("Inc action: ${store.dispatch(Action.Inc)}")
    }

    println("Dec action: ${store.dispatch(Action.Dec)}")
}