/**
 *
 */
typealias Middleware<Action, State> = (Store<Action, State>) -> (Dispatcher<Action, State>) -> Dispatcher<Action, State>

/**
 *
 */
fun <Action, State> applyMiddleware(
    vararg middlewares: Middleware<Action, State>,
): StoreEnhancer<Action, State> {
    return { storeCreator ->
        // Enhanced StoreCreator
        { reducer, state ->

            val store: Store<Action, State> = storeCreator(reducer, state)

            val dispatcher = store.dispatch

            val chain = middlewares.map { middleware ->
                middleware(store)
            }

            val enhancedDispatcher = chain.reduce { acc, item ->
                { next ->
                    acc(item(next))
                }
            }

            store.dispatch = enhancedDispatcher(dispatcher)

            store
        }
    }
}

/**
 *
 */
abstract class AbstractMiddleware<Action, State> : Middleware<Action, State> {

    override fun invoke(
        store: Store<Action, State>,
    ): (Dispatcher<Action,State>) -> Dispatcher<Action, State> = { next ->
        { action ->
            invoke(store, next, action)
        }
    }

    abstract fun invoke(store: Store<Action, State>, next: Dispatcher<Action,State>, action: Action): State
}

/**
 *
 */
fun <Action, State> createMiddleware(
    block: (Store<Action, State>, Dispatcher<Action, State>, Action) -> State
): Middleware<Action, State> = { store ->
    { next ->
        { action ->
            block(store, next, action)
        }
    }
}
