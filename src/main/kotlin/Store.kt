typealias Dispatcher<Action, State> = (Action) -> State
typealias Reducer<Action, State> = (Action, State) -> State

/**
 *
 */
class Store<Action, State>(
    private val reducer: Reducer<Action, State>,
    initialState: State,
) {

    var state: State = initialState
        private set


    var dispatch: Dispatcher<Action, State> = { action ->

        reducer(action, state).also {
            state = it
        }
    }
        internal set
}