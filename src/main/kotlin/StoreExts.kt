typealias StoreCreator<Action, State> = (Reducer<Action, State>, State) -> Store<Action, State>
typealias StoreEnhancer<Action, State> = (StoreCreator<Action, State>) -> StoreCreator<Action, State>

/**
 *
 */
fun <Action, State> combineReducers(
    vararg reducers: Reducer<Action, State>,
): Reducer<Action, State> = { action, state ->
    reducers.fold(state) { acc, reducer ->
        reducer(action, acc)
    }
}

/**
 * Creates store from [reducers] set, [preloadedState] and [storeEnhancer].
 */
fun <Action, State> makeStore(
    reducers: LinkedHashSet<Reducer<Action, State>>,
    preloadedState: State,
    storeEnhancer: StoreEnhancer<Action, State>? = null,
): Store<Action, State> {

    val storeCreator: StoreCreator<Action, State> = { reducer, state ->
        Store(reducer, state)
    }

    val finalStoreCreator = storeEnhancer?.invoke(storeCreator) ?: storeCreator

    return finalStoreCreator(
        combineReducers(*reducers.toTypedArray()),
        preloadedState,
    )
}
